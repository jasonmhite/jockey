import mpv
import paho.mqtt.client as mqtt
import feedparser
from pulsectl import Pulse

from flask import Flask
from flask_socketio import SocketIO, send, emit, Namespace

import json

MQTT_SERVER = "raspi"
MQTT_PORT = 1883
MQTT_TO = 60
#MQTT_TOPIC = "controllers/music0"
MQTT_TOPIC = "controllers/music1"

MQTT_PLAY = MQTT_TOPIC + "/play"
MQTT_STOP = MQTT_TOPIC + "/stop"
MQTT_VOL = MQTT_TOPIC + "/volume"

MQTT_GET_STATE = MQTT_TOPIC + "/status"
MQTT_STATE = MQTT_TOPIC + "/state"

WCPE = "http://audio-mp3.ibiblio.org:8000/wcpe.mp3"
NPR_NEWS = "http://www.npr.org/rss/podcast.php?id=500005"

state = "off"

app = Flask(__name__)

def logger(loglevel, component, message):
    # print('[{} {}: {}]'.format(loglevel, component, message))
    return

def spawn_player():
    P = mpv.MPV(log_handler=logger)
    return P

Player = spawn_player()

def play_wcpe():
    print("Play: WCPE")
    stop()
    Player.play(WCPE)

    global state
    state = "wcpe"

def play_npr():
    print("Play: NPR")
    stop()
    feed = feedparser.parse(NPR_NEWS)
    url = feed['entries'][0]['links'][0]['href']

    Player.play(url)

    global state
    state = "npr"

def stop():
    print("Stop")
    global Player
    Player.terminate()

    Player = spawn_player()

    global state
    state = "off"

def get_state():
    with Pulse("info-fetcher") as pulse:
        default_sink_name = pulse.server_info().default_sink_name

        for sink in pulse.sink_list():
            if sink.name == default_sink_name:
                volumes = sink.volume.value_flat

    out = {
        "default_sink": default_sink_name,
        "volume": volumes,
        "state": state,
    }

    return out

socketio = SocketIO(app)

@app.route('/st')
def poll():
    return json.dumps(get_state())

with open("frontend/index.html") as f:
    page = f.read()

@app.route('/')
def serve_page():
    return page

# @socketio.on("connect")
# def handle_connect(message):
    # print("Connection received")

# @socketio.on('message')
# def handle_message(message):
    # print('recieved: {}'.format(message))

# @socketio.on("request_state")
# def handle_request_state(data):
    # send(json.dumps(get_state()), namespace="/state")

handlers = {b"wcpe": play_wcpe, b"npr": play_npr}

class JockeyNamespace(Namespace):
    def on_request_status(self):
        emit("current_status", get_state())

    def on_connect(self):
        print("Client connected")

    def on_disconnect(self):
        print("Client disconnected")

    def on_set_volume(self, data):
        vol = data["data"]
        set_vol(vol)
        emit("current_status", get_state())

    def on_play(self, data):
        key = data["data"].encode()
        if key in handlers:
            handlers[key]()
        emit("current_status", get_state())

    def on_stop(self):
        stop()
        emit("current_status", get_state())


client = mqtt.Client()

def on_connect(client, userdata, flags, rc):
    print("Connected to MQTT")

    # for top in [MQTT_PLAY, MQTT_STOP, MQTT_VOL]:
        # client.subscribe(top)

    client.subscribe(MQTT_TOPIC + "/#")

def set_vol(vol):
    # print("Set volume: {}".format(vol))

    if vol <= 1.0 and vol >= 0:
        with Pulse('volume-increaser') as pulse:
            for sink in pulse.sink_list():
                pulse.volume_set_all_chans(sink, vol)

        print("Volume to {}%".format(int(vol * 100)))
    else:
        print("Volume {} is invalid".format(vol))

def on_message(client, userdata, msg):
    data = msg.payload
    print("{}: {}".format(msg.topic, data))

    try:
        if msg.topic == MQTT_PLAY:
            if data in handlers:
                handlers[data]()
                socketio.emit("current_status", get_state())
            else: print("Play {}: unknown".format(data))

        elif msg.topic == MQTT_VOL:
            vol = float(data)
            set_vol(vol)
            socketio.emit("current_status", get_state())

        elif msg.topic == MQTT_GET_STATE:
            client.publish(MQTT_STATE, json.dumps(get_state()))
            socketio.emit("current_status", get_state())

        elif msg.topic == MQTT_STOP:
            stop()
            socketio.emit("current_status", get_state())
        elif msg.topic == MQTT_STATE:
            return

        else:
            print("Command unknown")
    except Exception as e:
        print("Error handling message: {}".format(e))

client.on_connect = on_connect
client.on_message = on_message

client.connect(MQTT_SERVER, MQTT_PORT, MQTT_TO)

socketio.on_namespace(JockeyNamespace("/socket"))

client.loop_start()
socketio.run(app, host="0.0.0.0", port=80)
